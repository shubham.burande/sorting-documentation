# Sorting Algorithms

 **Sorting Algorithm** is used to rearrange a given array or list elements according to a **comparison operator** on the elements. 
The comparison operator is used to decide the new order of element in the respective data structure.

For example: The below list of characters is sorted in increasing order of their ASCII values. That is, the character with lesser ASCII value will be placed first than the character with higher ASCII value.


![array sort](https://static.javatpoint.com/programs/images/java-program-to-sort-the-elements-of-an-array-in-ascending-order.png)
<br><br>
## TYPES OF SORTS

1. **Selection sort** 
1. **Bubble sort** 
1. **Insertion sort**

<br><br><br><br>


 ## 1. Selection sort
The selection sort algorithm sorts an array by repeatedly finding the **minimum element** (considering ascending order) from **unsorted part** and putting it at the beginning. The algorithm maintains two sub-arrays in a given array.

1) The sub-array which is already sorted.
2) Remaining sub-array which is unsorted.

In every iteration of selection sort, the **minimum element** (considering ascending order) from the unsorted sub-array is picked and moved to the sorted sub-array.
<br>
### flowchart for selection sort

![array sort](https://www.c-programming-simple-steps.com/images/xSelectionSort.png.pagespeed.ic.GQoGt6TJ0y.webp)
<br><br><br>
 ### C program
```C
#include <stdio.h> 
  
void swap(int *xp, int *yp) 
{ 
    int temp = *xp; 
    *xp = *yp; 
    *yp = temp; 
} 
  
void selectionSort(int arr[], int n) 
{ 
    int i, j, min_idx; 
  
    // One by one move boundary of unsorted sub-array
 
    for (i = 0; i < n-1; i++) 
    { 
        // Find the minimum element in unsorted array 
        min_idx = i; 
        for (j = i+1; j < n; j++) 
          if (arr[j] < arr[min_idx]) 
            min_idx = j; 
  
        // Swap the found minimum element with the first element 
        swap(&arr[min_idx], &arr[i]); 
    } 
} 
  
/* Function to print an array */
void printArray(int arr[], int size) 
{ 
    int i; 
    for (i=0; i < size; i++) 
        printf("%d ", arr[i]); 
    printf("\n"); 
} 
  
// Driver program to test above functions 
int main() 
{ 
    int arr[] = {64, 25, 12, 22, 11}; 
    int n = sizeof(arr)/sizeof(arr[0]); 
    printf("unsorted array is \n");
    printArray(arr, n); 
    selectionSort(arr, n); 
    printf("\nSorted array: \n"); 
    printArray(arr, n); 
    return 0; 
} 

```
### Output
```
unsorted array:                                                            
64 25 12 22 11                                                         

Sorted array:                                                                 
11 12 22 25 64 
```

### Illustration
![Illustration of insertion sort](https://he-s3.s3.amazonaws.com/media/uploads/2888f5b.png)

<br><br><br><br><br>

 ## 2. Bubble sort

<br>

Bubble Sort is the simplest sorting algorithm that works by repeatedly swapping the adjacent elements if they are in wrong order.
In this algorithm the **largest number is bubbled out to the end** of the array in each pass .
<br><br>
Example:
First Pass:<br>
( 5 1 4 2 8 ) –> ( 1 5 4 2 8 ), Here, algorithm compares the first two elements, and swaps since 5 > 1.<br>
( 1 5 4 2 8 ) –>  ( 1 4 5 2 8 ), Swap since 5 > 4<br>
( 1 4 5 2 8 ) –>  ( 1 4 2 5 8 ), Swap since 5 > 2<br>
( 1 4 2 5 8 ) –> ( 1 4 2 5 8 ), Now, since these elements are already in order (8 > 5), algorithm does not swap them.
<br><br>
Second Pass:<br>
( 1 4 2 5 8 ) –> ( 1 4 2 5 8 )<br>
( 1 4 2 5 8 ) –> ( 1 2 4 5 8 ), Swap since 4 > 2<br>
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )<br>
( 1 2 4 5 8 ) –>  ( 1 2 4 5 8 )<br>
Now, the array is already sorted, but our algorithm does not know if it is completed. The algorithm needs one whole pass without any swap to know it is sorted.
<br><br>
Third Pass:<br>
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )<br>
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )<br>
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )<br>
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )<br>
<br><br>
### Bubble sort algorithm

![bubble sort](https://www.c-programming-simple-steps.com/images/xbubble-sort.png.pagespeed.ic.sNif7psMZJ.webp)

### C program
```C
// C program for implementation of Bubble sort 
#include <stdio.h> 

void swap(int *xp, int *yp) 
{ 
	int temp = *xp; 
	*xp = *yp; 
	*yp = temp; 
} 

// A function to implement bubble sort 
void bubbleSort(int arr[], int n) 
{ 
int i, j; 
for (i = 0; i < n-1; i++)	 

	// Last i elements are already in place 
	for (j = 0; j < n-i-1; j++) 
		if (arr[j] > arr[j+1]) 
			swap(&arr[j], &arr[j+1]); 
} 

/* Function to print an array */
void printArray(int arr[], int size) 
{ 
	int i; 
	for (i=0; i < size; i++) 
		printf("%d ", arr[i]); 
	printf("\n"); 
} 

// Driver program to test above functions 
int main() 
{ 
	int arr[] = {64, 34, 25, 12, 22, 11, 90}; 
	int n = sizeof(arr)/sizeof(arr[0]); 
	bubbleSort(arr, n); 
	printf("Sorted array: \n"); 
	printArray(arr, n); 
	return 0; 
} 


```

### Output
```
Sorted array:
11 12 22 25 34 64 90
```
<br>

### Illustration

![bubble sort](https://cdn.programiz.com/sites/tutorial2program/files/Bubble-sort-0.png)

<br><br><br><br><br>
---
 ## 3. Insertion sort
 
<br>

Insertion sort is a simple sorting algorithm that works similar to the way you sort playing cards in your hands. The array is virtually split into a **sorted and an unsorted part**. Values from the unsorted part are picked and placed at the correct position in the sorted part.
<br>
Algorithm
To sort an array of size n in ascending order:<br>
1: Iterate from arr[1] to arr[n] over the array.<br>
2: Compare the current element (key) to its predecessor.<br>
3: If the key element is smaller than its predecessor, compare it to the elements before. **Move the greater elements one position up** to make space for the swapped element.
<br><br>

### Insertion sort flowchart

<br>

![Insertion sort](https://www.includehelp.com/algorithms/images/insertion-sort-flowchart.jpg)


<br><br>

### C program

``` C 

// C program for insertion sort 
#include <math.h> 
#include <stdio.h> 

/* Function to sort an array using insertion sort*/
void insertionSort(int arr[], int n) 
{ 
	int i, key, j; 
	for (i = 1; i < n; i++) { 
		key = arr[i]; 
		j = i - 1; 

		/* Move elements of arr[0..i-1], that are 
		greater than key, to one position ahead 
		of their current position */
		while (j >= 0 && arr[j] > key) { 
			arr[j + 1] = arr[j]; 
			j = j - 1; 
		} 
		arr[j + 1] = key; 
	} 
} 

// A utility function to print an array of size n 
void printArray(int arr[], int n) 
{ 
	int i; 
	for (i = 0; i < n; i++) 
		printf("%d ", arr[i]); 
	printf("\n"); 
} 

/* Driver program to test insertion sort */
int main() 
{ 
	int arr[] = { 12, 11, 13, 5, 6 }; 
	int n = sizeof(arr) / sizeof(arr[0]); 
      printf("unsorted array:");
      printArray(arr, n)
	insertionSort(arr, n); 
      printf("Sorted array is");
	printArray(arr, n); 

	return 0; 
} 

```

### Output
```
unsorted array:                                                                                                     
64 25 12 22 11                                                                                                        
                                                                                                                      
Sorted array:                                                                                                         
11 12 22 25 64                                                                                                        
                     
```

### Illustration 

![illustration of insertion sort](https://media.geeksforgeeks.org/wp-content/uploads/insertionsort.png)
<br>

<br><br><br><br><br>

## References
<br>

1.  https://www.geeksforgeeks.org/sorting-algorithms/ <br>
1.  https://www.geeksforgeeks.org/insertion-sort/<br>
1.  https://www.geeksforgeeks.org/bubble-sort/ <br>
1.  https://www.geeksforgeeks.org/selection-sort/ <br>
1. https://gist.githubusercontent.com/bradtraversy/547a7bbf35ffba1561706e161a50b05a/raw/aed91bd0c570b240761bab7e67510a0817daaa8c/sample.md <br>



